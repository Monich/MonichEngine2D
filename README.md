#Monich Engine 2D
Hello, this is my 2D Engine, created from scratch, using glew, glfw libs, supported by DevIL and freetype so far. 

Feel free to use it, just add me somewhere in the credits.

#How to use
In your cmake file:
1. Include MonichEngine2D.cmake file
2. Add /src folder into included_directiories
3. Link your executable against MonichEngine2D and MSH projects
4. Steps to launch the engine from code are in MonichEngine.h file
5. Have fun!

Created by Monich @2017