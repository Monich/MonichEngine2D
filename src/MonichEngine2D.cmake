# Main MonichEngine2D cmake file

# Options
option(NO_GIT "Don`t generate GIT revision project" OFF)
option(USE_CONSOLE "Prepare engine for using console output." ON)

# Some files are generated - they are being saved in build/generated folder 
# @TODO - build isnt the best folder - should be any CMAKE_BUILD folder
include_directories("build/generated")

# Main engine project
file(GLOB_RECURSE INCS "${MonichEngine2D_Path}/src/monich2d/*.h")
file(GLOB_RECURSE SRCS "${MonichEngine2D_Path}/src/monich2d/*.cpp")
add_library(ME2D ${SRCS} ${INCS})

# Shaders
file(GLOB_RECURSE INCS "${MonichEngine2D_Path}/src/monich2dshaders/*.h")
file(GLOB_RECURSE SRCS "${MonichEngine2D_Path}/src/monich2dshaders/*.cpp")
add_library(ME2SH ${SRCS} ${INCS})

# Helpers
file(GLOB_RECURSE INCS "${MonichEngine2D_Path}/src/monich2dhelpers/*.h")
file(GLOB_RECURSE SRCS "${MonichEngine2D_Path}/src/monich2dhelpers/*.cpp")
add_library(ME2H ${SRCS} ${INCS})

# Definitions
add_definitions(-DGLEW_STATIC -DFT2_BUILD_LIBRARY -DIL_STATIC_LIB -DJPEGSTATIC -DIL_NO_EXTLIBS)

if(USE_CONSOLE)
	add_definitions(-DM2D_USE_CONSOLE)
endif(USE_CONSOLE)


# Libraries assisting MonichEngine2D

# glfw
file(GLOB_RECURSE INCS_GLFW "${MonichEngine2D_Path}/src/glfw/*.h")
file(GLOB_RECURSE SRCS_GLFW "${MonichEngine2D_Path}/src/glfw/*.c")

# glew
file(GLOB_RECURSE INCS_GLEW "${MonichEngine2D_Path}/src/glew/*.h")
file(GLOB_RECURSE SRCS_GLEW "${MonichEngine2D_Path}/src/glew/*.c")

# freetype
file(GLOB_RECURSE INCS_FT "${MonichEngine2D_Path}/src/font/freetype/ft2build.h" src/font/freetype/*/*.h)
set(SRCS_FT
  ${MonichEngine2D_Path}/src/font/freetype/autofit/autofit.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftbase.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftbbox.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftbdf.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftbitmap.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftcid.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftfntfmt.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftfstype.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftgasp.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftglyph.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftgxval.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftinit.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftlcdfil.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftmm.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftotval.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftpatent.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftpfr.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftstroke.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftsynth.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftsystem.c
  ${MonichEngine2D_Path}/src/font/freetype/base/fttype1.c
  ${MonichEngine2D_Path}/src/font/freetype/base/ftwinfnt.c
  ${MonichEngine2D_Path}/src/font/freetype/bdf/bdf.c
  ${MonichEngine2D_Path}/src/font/freetype/bzip2/ftbzip2.c
  ${MonichEngine2D_Path}/src/font/freetype/cache/ftcache.c
  ${MonichEngine2D_Path}/src/font/freetype/cff/cff.c
  ${MonichEngine2D_Path}/src/font/freetype/cid/type1cid.c
  ${MonichEngine2D_Path}/src/font/freetype/gzip/ftgzip.c
  ${MonichEngine2D_Path}/src/font/freetype/lzw/ftlzw.c
  ${MonichEngine2D_Path}/src/font/freetype/pcf/pcf.c
  ${MonichEngine2D_Path}/src/font/freetype/pfr/pfr.c
  ${MonichEngine2D_Path}/src/font/freetype/psaux/psaux.c
  ${MonichEngine2D_Path}/src/font/freetype/pshinter/pshinter.c
  ${MonichEngine2D_Path}/src/font/freetype/psnames/psnames.c
  ${MonichEngine2D_Path}/src/font/freetype/raster/raster.c
  ${MonichEngine2D_Path}/src/font/freetype/sfnt/sfnt.c
  ${MonichEngine2D_Path}/src/font/freetype/smooth/smooth.c
  ${MonichEngine2D_Path}/src/font/freetype/truetype/truetype.c
  ${MonichEngine2D_Path}/src/font/freetype/type1/type1.c
  ${MonichEngine2D_Path}/src/font/freetype/type42/type42.c
  ${MonichEngine2D_Path}/src/font/freetype/winfonts/winfnt.c
)

# DevIL and IL utilities
file(GLOB SRCS_DEVIL ${MonichEngine2D_Path}/src/IL/src-IL/*.cpp)
file(GLOB INCS_DEVIL ${MonichEngine2D_Path}/src/IL/src-IL/*.h build/generated/ILconfig.h ${MonichEngine2D_Path}/src/IL/devil_internal_exports.h ${MonichEngine2D_Path}/src/IL/il.h ${MonichEngine2D_Path}/src/IL/src-IL/ilu_error/*.h)
configure_file(${MonichEngine2D_Path}/src/IL/src-IL/config.h.cmake.in generated/ILconfig.h)
file(GLOB SRCS_ILU ${MonichEngine2D_Path}/src/IL/src-ILU/*.cpp)
file(GLOB INCS_ILU ${MonichEngine2D_Path}/src/IL/src-ILU/*.h ${MonichEngine2D_Path}/src/IL/devil_internal_exports.h ${MonichEngine2D_Path}/src/IL/ilu.h)
file(GLOB SRCS_ILUT ${MonichEngine2D_Path}/src/IL/src-ILUT/*.cpp)
file(GLOB INCS_ILUT ${MonichEngine2D_Path}/src/IL/src-ILUT/*.h ${MonichEngine2D_Path}/src/IL/devil_internal_exports.h ${MonichEngine2D_Path}/src/IL/ilut.h ${MonichEngine2D_Path}/src/IL/ilut_config.h)

# Create and link all those LIBS
add_library(ME2LIB
${SRCS_GLFW} ${INCS_GLFW}
${SRCS_GLEW} ${INCS_GLEW}
${SRCS_FT} ${INCS_FT}
${SRCS_DEVIL} ${INCS_DEVIL}
${SRCS_ILU} ${INCS_ILU}
${SRCS_ILUT} ${INCS_ILUT}
)

# These libraries are static - they can be linked against MonichEngine2D lib
target_link_libraries(ME2D ME2LIB)
target_link_libraries(ME2D OpenGL32.lib)

# Additional include - ft2build.h location
include_directories("${MonichEngine2D_Path}/src/font/freetype")

# Disable additional libs for DevIL (for now)
	set(IL_NO_EXR 1)
    set(IL_NO_PNG 1)
    set(IL_NO_TIF 1)
    set(IL_NO_JPG 1)
    set(IL_NO_JP2 1)
    set(IL_NO_MNG 1)
    set(IL_NO_LCMS 1)
    set(IL_USE_DXTC_NVIDIA 0)
    set(IL_USE_DXTC_SQUISH 0)

	
# Include revision generation file
include(${MonichEngine2D_Path}/src/revision/CMakeLists.txt)
