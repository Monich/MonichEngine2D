//Definitions of shared constants and types
#ifndef M2D_MSH_H
#define M2D_MSH_H

#include <monich2dshaders\MSHSimpleColored.h>
#include <monich2dshaders\MSHText.h>
#include <monich2dshaders\MSHTexture.h>

#endif // !M2D_DEFINES_H
