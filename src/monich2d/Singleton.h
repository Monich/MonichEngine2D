//SINGLETON CONSTRUCTOR
#ifndef M2D_SINGLETON_H
#define M2D_SINGLETON_H

class Singleton
{
public:
    template<class T>
    static T* getInstance();

protected:
    virtual Singleton* singletonInstance() = 0;
};

template<class T>
inline T * Singleton::getInstance()
{
    static T instance;
    return (T*)instance.singletonInstance();
}

#endif // !M2D_SINGLETON_H