#ifndef M2D_ENGINE_H
#define M2D_ENGINE_H

#include "Singleton.h"
#include "State.h"

#include <glm\glm.hpp>

#include <list>
#include <map>
#include <vector>

class Camera;
class Material;
class Mesh;
class Object;
class RenderTarget;
class Settings;
class Shader;
class Texture;

struct MouseEvent;

struct GLFWwindow;

// Check MonichEngine.cpp for enum (MonichEngineState)
typedef uint8 MEstate;

// Check MonichEngine.cpp for enum (MonichEngineErrors)
typedef uint8 MEerror;

//How to use:
/*
    0. OPTIONAL - Initialize the log by yourself - engine will do it in Init() if you dont
        - sLogInstance()->init()
    1. Validate the version of the engine using CheckVersion()
    2. Pass the Settings to the engine using ProvideSettings()
        - dont delete Settings - engine will do it yourself
        - if you want to add your own settings, just make your settings extend m2d Settings
    3. Initialize the engine using Init()
    4. Pass the State to the engine using SetNextState()
        - dont delete this too, even when changing states
    5. Initialize Materials
        - dont delete them too
        - how to do so:
            1) use StartMaterials() to declare how many materials you wanna use
            2) pass Materials using LoadMaterial()
            3) finish with FinishMaterialsLoading()
        - use can swap materials on fly - just initialize the one you're going to add and make sure you call prepare for every single object - the responsibility is on you!
    6. Launch()
    7. Be ready for Prepare on your state object 
        - load meshes and textures here 
        - pass Camera using SetCamera() and SetCameraUI()
*/


//The heart of the engine, collecting everything!
//Connects with the log and should be the only class using it
class MonichEngine : public Singleton
{
protected:
    //singleton stuff first
    MonichEngine();
    ~MonichEngine();
    Singleton* singletonInstance() { return this; }
    friend class Singleton;

public:
    void operator=(MonichEngine&) = delete;
    MonichEngine(MonichEngine&) = delete;

    // State Machine
    bool Init(ccstring windowName);
    void Terminate();
    bool SetNextState(State* nextState TODELETE); 

    // State checkers
    bool HasStateFlag(MEstate flag) const;

    bool IsInitialized() const;
    bool IsChecked() const;
    bool IsActivated() const;
    bool IsPreparing() const;
    bool IsForceQuiting() const;
    bool IsUIRendering() const;
    bool ErrorEncountered() const;
    bool MajorErrorEncountered() const;
    bool MinorErrorEncountered() const;

    bool IsReady() const;

    // Error Handling
    void ReportError(MEerror errorType);
    MEerror GetError() const;
    const char* GetErrorString() const;
    static const char* GetErrorStringFor(MEerror error);

	// Events 
	bool HasNextMouseEvent();
	MouseEvent* TODELETE NextMouseEvent();

    // Preparation methods
    Settings* NULLABLE ProvideSettings(Settings* settings NULLABLE TODELETE); // NULL can cause problems tho, you can erease the Settings that way tho
    bool CheckVersion(uint8 major, uint8 minor);
    void SetCamera(Camera* camera NULLABLE TODELETE);
    void SetUICamera(Camera* camera NULLABLE TODELETE);
	
	// Use only when necessary, can slow down engine a ton when called
	void RequestPreparationModeOn();
	void RequestPreparationModeOff();

    // Actual work
    void Launch();
    void ForceStop();
    void Render(Object* o);
	void Render(Object** o, uint32 objectCount);
    void RenderText(uint32 materialIndex);

    /// Content Manager

    // Helper Objects
    Settings* NULLABLE GetSettings();
    Camera* NULLABLE GetCamera();
    Camera* NULLABLE GetUICamera();
    GLFWwindow* NULLABLE GetWindow() const;

    // Mesh
	uint32 LoadMesh(ccstring meshName);
    Mesh* NULLABLE LoadMesh(uint32 index, ccstring meshName);
    Mesh* NULLABLE LoadMesh(uint32 index, Mesh* newMesh NULLABLE TODELETE, bool deleteOld); //will return old mesh if deleteOld is false
    Mesh* NULLABLE GetMesh(uint32 index);

    //Materials
    void StartMaterialsLoading(uint32 materialsCount);
    void LoadMaterial(Material* mat NULLABLE, uint32 index);
    bool FinishMaterialsLoading();

    void PassMaterialData(uint32 materialIndex, uint32 dataIndex, uint64 data);
    void PassMaterialData(uint32 materialIndex, uint32 dataIndex, float data);
    void PassMaterialData(uint32 materialIndex, uint32 dataIndex, void* data);

    //Textures
	uint32 LoadTexture(ccstring texName);
    Texture* NULLABLE LoadTexture(uint32 index, ccstring texName);
	Texture* NULLABLE LoadTexture(uint32 index, Texture* tex NULLABLE TODELETE, bool deleteOld);
	Texture* NULLABLE GetTexture(uint32 index);
	Texture* NULLABLE GetTexture(ccstring texName);

	//Render Target - null makes it screen and is default
	void SetRenderTarget(RenderTarget* rTarget, bool clear = false);
	void SetRenderViewPort(int32 x, int32 y, int32 w, int32 h);

private:

    /// FIELDS

    // State Machine
    MEstate stateFlag;
    MEerror errorType;
    StatePtr currentState;
    StatePtr nextState;

    // Helper Objects
    Settings* settings;
    Camera* camera;
    Camera* UICamera;

	// Event handling
	std::list<MouseEvent*> mouseEvents;

    // GPU data
    unsigned vertexArrayObjectID;
    GLFWwindow* window;

    // Graphic Engine Containers
    Shader** shaders;
    std::map<uint32, Mesh*> meshes;
    Material** materials;
    uint32 materialCount;
    uint32 currentMaterial;
    std::map<std::string, int> textureMap;
    std::vector<Texture*> textures;

    /// HELPERS

    // State Setters
    void _SetFlag(MEstate flag, bool on);
    void _SetInitialized(bool on = true);
    void _SetChecked(bool on = true);
    void _SetActivated(bool on = true);
    void _SetPreparing(bool on = true);
    void _SetShouldQuit(bool on = true);
    void _SetUIRendering(bool on = true);

	// Event Helpers
	void _PollMouseEvent(MouseEvent* TODELETE mEvent);

    // Initializers
    bool _InitGLFW(ccstring windowName);
    bool _InitGLEW();
    bool _InitDevIL();
    bool _InitShaders();
    bool _InitFonts();

    // Error helpers
    void _PrintErrorMessage();
    static bool _IsErrorMajor(MEerror errorType);

    // Ticking
    void _SwitchStates();
    bool _ShouldClose();

    void _Render(uint32 materialIndex, Object* o NULLABLE);

    // Graphical Helpers
    glm::mat4 * _GetMVP(Object* o NULLABLE);

    // Misc
    template<typename T>
    void _PassMaterialData(uint32 materialIndex, uint32 dataIndex, T data);
};

#define sEngine Singleton::getInstance<MonichEngine>()

#endif

template<typename T>
void MonichEngine::_PassMaterialData(uint32 materialIndex, uint32 dataIndex, T data)
{
    if (materialCount > materialIndex)
    {
        materials[materialIndex]->PassData(dataIndex, data);
    }
}
