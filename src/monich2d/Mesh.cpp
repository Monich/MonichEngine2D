#include "Mesh.h"

#include <glew\glew.h>

Mesh::Mesh() : buffers(NULL)
{}


Mesh::~Mesh()
{
    if (buffers)
    {
        glDeleteBuffers(3, &buffers[1]);
        delete[] buffers;
    }
}

MeshError Mesh::Init(ccstring meshName)
{
    std::vector<unsigned > indices;
    std::vector<glm::vec2> vertices;
    std::vector<glm::vec2> uvs;

    std::vector<unsigned> vertexIndices, uvIndices;
    std::vector<glm::vec2> temp_vertices;
    std::vector<glm::vec2> temp_uvs;

    FILE * file = fopen(meshName, "r");
    if (file == NULL) {
        return MESH_ERROR_FILE;
    }

    while (1) {

        char lineHeader[128];
        // read the first word of the line
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF)
            break; // EOF = End Of File. Quit the loop.

                   // else : parse lineHeader

        if (strcmp(lineHeader, "v") == 0) {
            glm::vec2 vertex;
            fscanf(file, "%f %f\n", &vertex.x, &vertex.y);
            temp_vertices.push_back(vertex);
        }
        else if (strcmp(lineHeader, "vt") == 0) {
            glm::vec2 uv;
            fscanf(file, "%f %f\n", &uv.x, &uv.y);
            temp_uvs.push_back(uv);
        }
        // I leave it in case i ever want to make 3D engine
        /*else if (strcmp(lineHeader, "vn") == 0) {
            glm::vec3 normal;
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
            temp_normals.push_back(normal);
        }*/
        else if (strcmp(lineHeader, "f") == 0) {
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
            if (matches != 9)
                return MESH_ERROR_FACE_FORMAT;

            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);
            uvIndices.push_back(uvIndex[0]);
            uvIndices.push_back(uvIndex[1]);
            uvIndices.push_back(uvIndex[2]);
            //normalIndices.push_back(normalIndex[0]);
            //normalIndices.push_back(normalIndex[1]);
            //normalIndices.push_back(normalIndex[2]);
        }
		else if (strcmp(lineHeader, "l") == 0) {
			unsigned int vertexIndex[2], uvIndex[2], normalIndex[2];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1]);
			if (matches != 6)
				return MESH_ERROR_LINE_FORMAT;

			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			//normalIndices.push_back(normalIndex[0]);
			//normalIndices.push_back(normalIndex[1]);
			//normalIndices.push_back(normalIndex[2]);
		}
        else {
            // Probably a comment, eat up the rest of the line
            char stupidBuffer[1000];
            fgets(stupidBuffer, 1000, file);
        }
    }

    fclose(file);

    // Indexing

    std::vector<glm::vec2> indicesUsed;

    for (unsigned int i = 0; i < vertexIndices.size(); i++)
    {
        unsigned int vertexIndex = vertexIndices[i];
        unsigned int uvIndex = uvIndices[i];
        //unsigned int normalIndex = normalIndices[i];

        glm::vec2 data = glm::vec2(vertexIndex, uvIndex);

        unsigned int pos = (find(indicesUsed.begin(), indicesUsed.end(), data) - indicesUsed.begin());

        if (pos >= indicesUsed.size())
        {//NOT FOUND
         // Get the attributes thanks to the index
            glm::vec2 vertex = temp_vertices[vertexIndex - 1];
            glm::vec2 uv = temp_uvs[uvIndex - 1];
            //glm::vec3 normal = temp_normals[normalIndex - 1];

            // Put the attributes in buffers
            indices.push_back((unsigned)(vertices.size()));
            vertices.push_back(vertex);
            uvs.push_back(uv);
            //normals.push_back(normal);

            indicesUsed.push_back(data);
        }
        else
        {//FOUND
            indices.push_back(pos);
        }
    }

    // All data ready, save it and pass it to GPU
    CreateGpuData(indices, vertices, uvs);

    return MESH_ERROR_NONE;
}

void Mesh::CreateGpuData(std::vector<unsigned>& indices, std::vector<glm::vec2>& vertices, std::vector<glm::vec2>& uvs)
{
    buffers = new MeshBuffer[MESH_FIELD_MAX];

    buffers[MESH_FIELD_INDICES] = indices.size();

    glGenBuffers(3, &buffers[1]);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[MESH_FIELD_VBO]);
    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[MESH_FIELD_IBO]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), &indices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[MESH_FIELD_UVS]);
    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * uvs.size(), &uvs[0], GL_STATIC_DRAW);
}

MeshBuffer Mesh::GetBuffer(MeshFieldIndex index)
{
	return buffers[index];
}
