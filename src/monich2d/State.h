#ifndef M2D_STATE_H
#define M2D_STATE_H

#include "defines.h"

#include <memory>


class State
{
public:
	virtual ~State() {};

    virtual void prepare() = 0;
	virtual void logic(uint32 diff) = 0;
	virtual void render(uint32 diff) = 0;
	virtual void renderUI(uint32 diff) {};
};

typedef std::shared_ptr<State> StatePtr;

#endif //!M2D_STATE_H