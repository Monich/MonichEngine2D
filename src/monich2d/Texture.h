#ifndef M2D_TEXTURE_H
#define M2D_TEXTURE_H

#include "defines.h"

#include <string>

typedef uint8 TextureError;
enum TextureErrors
{
    TEXTURE_ERROR_NONE,
    TEXTURE_ERROR_OVERRIDE,
    TEXTURE_ERROR_FILE_NOT_FOUND,
    TEXTURE_ERROR_CONVERSION_FAILED,
    TEXTURE_ERROR_MAX
};

//Class holding and executing all operations on textures
class Texture
{
	//texture pointer in GPU memory
    unsigned textureID;
public:
	Texture();
    TextureError Init(const char * path);
	TextureError Init(uint32 w, uint32 h, uint32 format, uint32 type);

    unsigned GetTexture();

	~Texture();
};

#endif //!M2D_TEXTURE_H
