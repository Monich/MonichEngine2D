//Log utility for Condottierie client
#ifndef M2D_LOG_H
#define M2D_LOG_H
#include <stdio.h>
#include "Singleton.h"

class Log : public Singleton
{
private:
    Log();
    ~Log();
    Singleton* singletonInstance() { return this; }
    friend class Singleton;

public:
    void operator=(Log&) = delete;
    Log(Log&) = delete;

    bool init();
    void terminate();

    void Out(const char* str, ...);

private:
    ///fields
    FILE* file;
};

//if you dont want to use standard singleton structure for logging use #define DONT_USE_M2D_LOG 
#ifndef DONT_USE_M2D_LOG

#define sLog Singleton::getInstance<Log>()->Out
#define sLogInstance Singleton::getInstance<Log>()

#endif //!DONT_USE_M2D_LOG

#endif //!M2D_LOG_H