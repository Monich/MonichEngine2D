#ifndef M2D_CAMERA_H
#define M2D_CAMERA_H

#include <glm\glm.hpp>

class Camera
{
public:
    glm::mat4& GetCameraView();
protected:
    glm::mat4 cameraMatrix;
};

#endif