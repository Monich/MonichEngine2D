#include "Object.h"

#define _USE_MATH_DEFINES
#include <math.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm\gtx\transform.hpp>


glm::mat4 * Object::PrepareModelMatrix(const glm::vec2& position, const glm::vec2& rotation, const glm::vec2& scale)
{
    using namespace glm;

    mat4 * matrix = new mat4();

	//scale
    *matrix = glm::scale(vec3(scale, 1)) * mat4(1);
	//rotation
	vec3 vertex = vec3(0, 0, 1);
	//degrees to radians
	float d_2_r = M_PI / 180;
	*matrix = rotate(rotation[0] * d_2_r, vertex) * (*matrix);
	vertex = vec3(0, 1, 0);
	*matrix = rotate(rotation[1] * d_2_r, vertex) * (*matrix);
	//transformation
	*matrix = translate(vec3(position, 0)) * (*matrix);

	return matrix;
}
