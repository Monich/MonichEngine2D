#include "Texture.h"

#include <IL\il.h>
#include <glew\glew.h>

Texture::Texture()
{
	textureID = 0;
}

TextureError Texture::Init(const char * path)
{
	unsigned int imageID;
	ilGenImages(1, &imageID);
	ilBindImage(imageID);

	//check for exstistence
    if (textureID != 0)
        return TEXTURE_ERROR_OVERRIDE;

	//validate file
    FILE* file;
    fopen_s(&file, path, "r");
    if (!file)
        return TEXTURE_ERROR_FILE_NOT_FOUND;

    fclose(file);

    //load file
	if (!ilLoadImage(path))
        return TEXTURE_ERROR_FILE_NOT_FOUND;

	//convert to rgb
    if (!ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE))
        return TEXTURE_ERROR_CONVERSION_FAILED;

	//send to gpu
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (GLuint)ilGetInteger(IL_IMAGE_WIDTH), (GLuint)ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGB, GL_UNSIGNED_BYTE, (GLuint*)ilGetData());

	//cleanup
	ilDeleteImages(1, &imageID);

	//parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	return TEXTURE_ERROR_NONE;
}

TextureError Texture::Init(uint32 w, uint32 h, uint32 format, uint32 type)
{
	//check for exstistence
	if (textureID != 0)
		return TEXTURE_ERROR_OVERRIDE;

	//send to gpu
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, type, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

GLuint Texture::GetTexture()
{
	return textureID;
}

Texture::~Texture()
{
	glDeleteTextures(1, &textureID);
}
