#ifndef M2D_MESH_H
#define M2D_MESH_H

#include "defines.h"

#include <glm\glm.hpp>

#include <vector>

typedef uint8 MeshFieldIndex;
enum MeshFields 
{
    MESH_FIELD_INDICES,
    MESH_FIELD_VBO,
    MESH_FIELD_IBO,
    MESH_FIELD_UVS,
    MESH_FIELD_MAX
};

typedef uint8 MeshError;
enum MeshErrors
{
    MESH_ERROR_NONE,
    MESH_ERROR_FILE,
    MESH_ERROR_FACE_FORMAT,
	MESH_ERROR_LINE_FORMAT,
    MESH_ERROR_MAX
};

typedef unsigned MeshBuffer;

//Class holding data about single renderable mesh
class Mesh
{
public:
    Mesh();
    ~Mesh();

    MeshError Init(ccstring meshName);
    void CreateGpuData(std::vector<unsigned>& indices, std::vector<glm::vec2>& vertices, std::vector<glm::vec2>& uvs);

	MeshBuffer GetBuffer(MeshFieldIndex index);

private:
    MeshBuffer* buffers;
};

#endif M2D_MESH_H
