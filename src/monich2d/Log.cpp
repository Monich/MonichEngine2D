#include "Log.h"

#include <stdarg.h>
#include <iostream>
#include <ctime>

#define MAX_QUERY_LEN 32*1024

Log::Log() : file(NULL) 
{}

Log::~Log()
{
    terminate();
}

bool Log::init()
{
    if(!file)
        fopen_s(&file, "log.txt", "w");

    return file;
}

void Log::terminate()
{
    if(file)
        fclose(file);
}

void Log::Out(const char * str, ...)
{
    if (str && file)
    {
        time_t t = time(0);   // get time now
        struct tm * now = localtime(&t);
        fprintf(file, "[%d:%d:%d]: ", now->tm_hour, now->tm_min, now->tm_sec);

        va_list ap;
        va_start(ap, str);
        vfprintf(file, str, ap);

        //print to stdout if enabled too
#ifdef M2D_USE_CONSOLE
        vprintf(str, ap);
        printf("\n");
#endif

        va_end(ap);

        fprintf_s(file, "\n");
    }
}
