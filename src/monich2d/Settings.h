#ifndef M2D_SETTINGS_H
#define M2D_SETTINGS_H

#include "defines.h"

#include <glm\glm.hpp>

#include <fstream>

typedef int8 SettingsIndex;
enum BasicSettingsFields
{
    //UINTS
    SETTING_SCREEN_W,
    SETTING_SCREEN_H,
    SETTING_MAX,

    //CSTRINGS
    SETTING_FONT_ADDRESS    =0,
    SETTING_CSTRING_MAX
};

class Settings
{
public:
    Settings();
    ~Settings();

    bool Load();
    bool Save();
    void GenerateDefault();

    uint64 GetUnsignedData(SettingsIndex index);
    int64 GetSignedData(SettingsIndex index);
    float64 GetFloatData(SettingsIndex index);
    cstring NULLABLE GetCStringData(SettingsIndex index);

protected:
    void _loadBasic(std::ifstream& file);
    void _saveBasic(std::ofstream& file);

    //use this functions for extending the settings
    virtual void _load(std::ifstream& file) {};
    virtual void _save(std::ofstream& file) {};

    virtual uint16 _getFieldSize();
    virtual uint16 _getCStringFieldSize();

    uint64 loadUnsigned(std::ifstream& file);
    int64 loadSigned(std::ifstream& file);
    float64 loadFloat(std::ifstream& file);

    cstring loadCString(std::ifstream& file);

private:
    BigField* fields;
    cstring* cstrings;
    bool saved;
    bool loaded;

    template<typename T>
    T _loadField(std::ifstream& file);
};

#endif
