#ifndef M2D_OBJECT_H
#define M2D_OBJECT_H

#include "Mesh.h"
#include "defines.h"

#include <glm\glm.hpp>

//Basic object, extend this to create renderable 'thing'
class Object
{
protected:
	static glm::mat4 * PrepareModelMatrix(const glm::vec2& position, const glm::vec2& rotation, const glm::vec2& scale);
public:
	virtual ~Object() {};

	virtual glm::mat4 * TODELETE GetModelMatrix() = 0;
	virtual Mesh* GetMesh() const = 0;
	virtual uint32 GetMaterial() = 0;
};

#endif
