#include "RenderTarget.h"

#include <glew\glew.h>

RenderTarget::RenderTarget(uint32 w, uint32 h, uint32 format, uint32 type)
{
	glGenFramebuffers(1, &framebufferId);
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);

	// texture
	texture = new Texture();
	TextureError err = texture->Init(w, h, format, type);

	glBindTexture(GL_TEXTURE_2D ,texture->GetTexture());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//! add depth buffer if 3d

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture->GetTexture(), 0);

	unsigned drawBuffer = GL_COLOR_ATTACHMENT0;
	glDrawBuffers(1, &drawBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

RenderTarget::~RenderTarget()
{
	glDeleteFramebuffers(1, &framebufferId);
	delete texture;
}

bool RenderTarget::CheckError()
{
	return glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE;
}

unsigned RenderTarget::GetBuffer() const
{
	return framebufferId;
}

Texture * RenderTarget::GetTexture() const
{
	return texture;
}
