//Definitions of shared constants and types
#ifndef M2D_DEFINES_H
#define M2D_DEFINES_H

#ifdef _MSC_VER 
#pragma warning(disable : 4800) // warning for while(0)
#endif

#include <cstdint>
#include <ctime>

// types

typedef uint8_t uint8;
typedef int8_t int8;

typedef uint16_t uint16;
typedef int16_t int16;

typedef uint32_t uint32;
typedef int32_t int32;

typedef uint64_t uint64;
typedef int64_t int64;

typedef float float32;
typedef double float64;

typedef const char* ccstring;
typedef char* cstring;

union BigField
{
    uint64 unsign;
    int64 sing;
    float64 floa;
};

// Defines for the engine

// Args or return values marked as NULLABLE can be null, and it should be checked
#define NULLABLE 
// Args or return values marked as TODELETE means that the pointer should be deleted by user 
// As an arg: pointer will be deleted by the class called
// Return value: pointer should be deleted by the function reciever
#define TODELETE

#define START_LOAD(S) clock_t beginTime = clock();sLog(S)
#define END_LOAD(S) clock_t endTime = clock();double elapsed = double(endTime - beginTime) / CLOCKS_PER_SEC;sLog(S, elapsed)

#define DELETEVECTOR(V) for(auto it = V.begin(); it != V.end(); it++) delete (*it); V.clear()
#define DELETEMAP(V) for(auto it = V.begin(); it != V.end(); it++) delete (*it).second; V.clear()

#define FOREACH(V) for(auto it = V.begin(); it != V.end(); it++)

//prepares terminated cString, remember to delete it after you used it
cstring TODELETE prepareCString(uint32 lenght);
cstring TODELETE copyCString(ccstring other);

#endif // !M2D_DEFINES_H
