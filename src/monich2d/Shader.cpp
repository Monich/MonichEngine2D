#include "Shader.h"

#include "MSH.h"

#include <glew\glew.h>

enum ShaderSources
{
    SHADER_NONE,

    SHADER_VERTEX_SIMPLE,
    SHADER_VERTEX_TEXTURE,
    SHADER_VERTEX_SIMPLE_PARTICLE,
    SHADER_VERTEX_TEXT,

    SHADER_FRAG_ONE_COLOR,
    SHADER_FRAG_TEXTURE,
    SHADER_FRAG_TEXT,
};

Shader::~Shader()
{
    if (buffer)
    {
        glDeleteProgram(buffer[0]);
        delete buffer;
    }
}

//credit goes to opengl_tutorial.org
//changes by Monich
ShaderError Shader::InitShader(cstring& errorString)
{
    int result;

    // check if shader is initialized
    if (!buffer)
        return SHADER_ERROR_NON_INITIALIZED;

    //buffer[0] = programId
    //it`s better not to leave that uninitialized
    buffer[0] = 0;

    // Get the codes for shaders
    ccstring vertexShaderCode = _GetVertexCode();
    ccstring fragmentShaderCode = _GetFragmentCode();

    // If one of codes is empty - abort!
    if (!(vertexShaderCode && fragmentShaderCode))
        return SHADER_ERROR_INVALID_ID;

    // Create the shaders on the gpu
    uint64 vertexShaderGPUID = glCreateShader(GL_VERTEX_SHADER);
    uint64 fragmentShaderGPUID = glCreateShader(GL_FRAGMENT_SHADER);

    // Compile Vertex Shader
    glShaderSource(vertexShaderGPUID, 1, &vertexShaderCode, NULL);
    glCompileShader(vertexShaderGPUID);

    // Check Vertex Shader
    glGetShaderiv(vertexShaderGPUID, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        // failed, retrieve error string and abort
        _getErrorStringForShader(errorString, vertexShaderGPUID);

        glDeleteShader(vertexShaderGPUID);
        glDeleteShader(fragmentShaderGPUID);
        return SHADER_ERROR_VERTEX_SHADER_COMPILATION_FAILED;
    }

    // Compile Fragment Shader
    glShaderSource(fragmentShaderGPUID, 1, &fragmentShaderCode, NULL);
    glCompileShader(fragmentShaderGPUID);

    // Check Fragment Shader
    glGetShaderiv(fragmentShaderGPUID, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        // failed, retrieve error string and abort
        _getErrorStringForShader(errorString, vertexShaderGPUID);

        glDeleteShader(vertexShaderGPUID);
        glDeleteShader(fragmentShaderGPUID);
        return SHADER_ERROR_FRAGMENT_SHADER_COMPILATION_FAILED;
    }

    // Link the program
    buffer[0] = glCreateProgram();
    glAttachShader(buffer[0], vertexShaderGPUID);
    glAttachShader(buffer[0], fragmentShaderGPUID);
    glLinkProgram(buffer[0]);

    // Check the program
    glGetProgramiv(buffer[0], GL_LINK_STATUS, &result);
    if (result == GL_FALSE)
    {
        int logLenght;
        glGetProgramiv(buffer[0], GL_INFO_LOG_LENGTH, &logLenght);

        //this will get deleted in upper function
        errorString = prepareCString(logLenght);
        glGetProgramInfoLog(buffer[0], logLenght, NULL, &errorString[0]);
    }

    //detach and delete shaders no matter the result is
    glDetachShader(buffer[0], vertexShaderGPUID);
    glDetachShader(buffer[0], fragmentShaderGPUID);

    glDeleteShader(vertexShaderGPUID);
    glDeleteShader(fragmentShaderGPUID);

    return result == GL_FALSE ? SHADER_ERROR_PROGRAM_LINK_FAILED : SHADER_ERROR_NONE;
}

void Shader::Use()
{
    glUseProgram(buffer[0]);
}

unsigned Shader::GetBuffer(uint8 index) const
{
    return buffer[index];
}

ShaderType Shader::GetType() const
{
    return type;
}

ccstring Shader::GetName() const
{
    return GetName(GetType());
}

ccstring Shader::GetName(ShaderType type)
{
    switch (type)
    {
    case SHADER_SIMPLE_COLOR:
        return "SimpleColorShader";
    case SHADER_TEXTURE:
        return "SimpleTextureShader";
    case SHADER_TEXT:
        return "TextShader";
    default:
        return "Not a valid shader id";
    }
}

MSHSimpleColored * NULLABLE Shader::ToSimpleColoredMSH()
{
    return castTo<MSHSimpleColored>();
}

MSHText * NULLABLE Shader::ToTextMSH()
{
    return castTo<MSHText>();
}

MSHTexture * NULLABLE Shader::ToTextureMSH()
{
    return castTo<MSHTexture>();
}

Shader * Shader::CreateNewInstance(ShaderType type)
{
    switch (type)
    {
    case SHADER_SIMPLE_COLOR:
        return new MSHSimpleColored();
    case SHADER_TEXTURE:
        return new MSHTexture();
    case SHADER_TEXT:
        return new MSHText();
    default:
        return NULL;
    }
}

Shader::Shader(const ShaderType type) : type(type), buffer(NULL)
{}

void Shader::_getErrorStringForShader(cstring & errorString, int shaderId) const
{
    int logLenght;
    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logLenght);

    //this will get deleted in upper function
    errorString = prepareCString(logLenght);
    glGetShaderInfoLog(shaderId, logLenght, NULL, &errorString[0]);
}

template<class T>
T* Shader::castTo()
{
    return reinterpret_cast<T*>(this);
}

ccstring NULLABLE Shader::_GetVertexCode() const
{
    return _GetCode(_GetVertexId());
}

ccstring NULLABLE Shader::_GetFragmentCode() const
{
    return _GetCode(_GetFragmentId());
}

ShaderCodeId Shader::_GetVertexId() const
{
    return _GetVertexId(GetType());
}

ShaderCodeId Shader::_GetFragmentId() const
{
    return _GetFragmentId(GetType());
}

ccstring NULLABLE Shader::_GetCode(ShaderType type)
{
    switch (type)
    {
    case SHADER_VERTEX_SIMPLE:
        return "#version 330 core\nin vec2 aVBOHandle;uniform mat4 uMVPMatrix;void main(){gl_Position=uMVPMatrix*vec4(aVBOHandle,0,1);}";
    case SHADER_VERTEX_TEXTURE:
        return "#version 330 core\nin vec3 aVBOHandle;uniform mat4 uMVPMatrix;in vec2 aUV;out vec2 iaUV;void main(){gl_Position = uMVPMatrix * vec4(aVBOHandle, 1);	iaUV = aUV;}";
    case SHADER_VERTEX_SIMPLE_PARTICLE:
        return "#version 330 core\nin vec3 meshHandle;in vec3 centrePosition;uniform mat4 uVPMatrix;uniform vec3 uSize;void main(){vec3 position = meshHandle.xy + centrePosition*uSize;gl_Position = uVPMatrix * vec4(position, 1.0f);}";
    case SHADER_VERTEX_TEXT:
        return "#version 330 core\nin vec2 aVBOHandle;uniform mat4 uProjMatrix;void main(){gl_Position = uProjMatrix * vec4(aVBOHandle, 0, 1);}";
    case SHADER_FRAG_ONE_COLOR:
        return "#version 330 core\nuniform vec3 uColor;layout(location = 0) out vec3 color;void main(){color = uColor;}";
    case SHADER_FRAG_TEXTURE:
        return "#version 330 core\nin vec2 iaUV;uniform sampler2D uTextureUsed;layout(location = 0) out vec3 color;void main() {color = texture(uTextureUsed, iaUV).rgb;}";
    case SHADER_FRAG_TEXT:
        return "#version 330 core\nout vec3 color;void main() {color = vec3(0, 0, 1);}";
    default:
        return NULL;
    }
}

ShaderCodeId Shader::_GetVertexId(ShaderType type)
{
    switch (type)
    {
    case SHADER_SIMPLE_COLOR:
        return SHADER_VERTEX_SIMPLE;
    case SHADER_TEXTURE:
        return SHADER_VERTEX_TEXTURE;
    case SHADER_TEXT:
        return SHADER_VERTEX_TEXT;
    default:
        return SHADER_NONE;
    }
}

ShaderCodeId Shader::_GetFragmentId(ShaderType type)
{
    switch (type)
    {
    case SHADER_SIMPLE_COLOR:
        return SHADER_FRAG_ONE_COLOR;
    case SHADER_TEXTURE:
        return SHADER_FRAG_TEXTURE;
    case SHADER_TEXT:
        return SHADER_FRAG_TEXT;
    default:
        return SHADER_NONE;
    }
}