#include "MonichEngine.h"

#include "Camera.h"
#include "Log.h"
#include "Material.h"
#include "Mesh.h"
#include "MouseEvent.h"
#include "RenderTarget.h"
#include "Settings.h"
#include "Shader.h"
#include "Texture.h"

#include <monich2dshaders\MSHText.h>
#include <revision\revision.h>

#include <glew\glew.h>
#include <glfw\glfw3.h>
#include <IL/il.h>
#include <IL/ilu.h>

// Check MonichEngine.h for according typedef (MEstate)
enum MonichEngineState
{ //FLAGS!
    ENGINE_STATE_INITIALIZED = 1 << 0,
    ENGINE_STATE_CHECKED = 1 << 1, // the engine version has been checked
    ENGINE_STATE_ACTIVATED = 1 << 2,
    ENGINE_STATE_PREPARING = 1 << 3,
    ENGINE_STATE_SHOULD_QUIT = 1 << 4,
    ENGINE_STATE_UI_RENDERING = 1 << 5,
};

// Check MonichEngine.h for according typedef (MEerror)
enum MonichEngineErrors
{
    ME_ERROR_NONE = 0,

    ME_WARNING_CAMERA_UINITIALIZED,
    ME_WARNING_CONFIG_FILE_NOT_SAVED,
    ME_WARNING_LOG_INIT_FAILED,
    ME_WARNING_NO_CONFIG_FILE,
    ME_WARNING_NOT_TERMINATED,
    ME_WARNING_SETTINGS_UNINITIALIZED,
    ME_WARNING_VERSION_NOT_CHECKED,

    ME_ERROR_MAJOR_STAMP, //all errors under this are minor, above are major

    ME_ERROR_DEVIL_INIT,
    ME_ERROR_GLEW_INIT,
    ME_ERROR_GLFW_INIT,
    ME_ERROR_GLFW_WINDOW_FAILED,
    ME_ERROR_MATERIALS_INIT_FAILED,
    ME_ERROR_REQUESTED_MESH_NOT_IN_PREPARING_STATE,
    ME_ERROR_SHADER_FAILED_VARIABLE_LOAD,
    ME_ERROR_STATE_UNINITIALIZED,
    ME_ERROR_VERSION_INVALID,

    ME_ERROR_FREETYPE_ZERO = ME_ERROR_VERSION_INVALID,
    ME_ERROR_FREETYPE_LAST = ME_ERROR_FREETYPE_ZERO + FREETYPE_ERROR_MAX - 1,
    ME_ERROR_MESH_ZERO = ME_ERROR_FREETYPE_LAST,
    ME_ERROR_MESH_LAST = MESH_ERROR_MAX + ME_ERROR_MESH_ZERO - 1,
    ME_ERROR_SHADER_ZERO = ME_ERROR_MESH_LAST,
    ME_ERROR_SHADER_LAST = SHADER_ERROR_MAX + ME_ERROR_SHADER_ZERO - 1,
    ME_ERROR_TEXTURE_ZERO = ME_ERROR_SHADER_LAST,
    ME_ERROR_TEXTURE_LAST = ME_ERROR_TEXTURE_ZERO + TEXTURE_ERROR_MAX - 1,

    ME_ERROR_MAX
};

MonichEngine::MonichEngine() :
    stateFlag(0), errorType(0), currentState(NULL), nextState(NULL),
    settings(NULL), camera(NULL), UICamera(NULL),
    vertexArrayObjectID(0), window(NULL),
    shaders(NULL), materials(NULL), materialCount(0)
{}

MonichEngine::~MonichEngine()
{
    if (IsInitialized())
    {
        ReportError(ME_WARNING_NOT_TERMINATED);
        terminate();
    }
}

bool MonichEngine::Init(ccstring windowName)
{
    bool success;

    if (!sLogInstance->init())
        ReportError(ME_WARNING_LOG_INIT_FAILED);

    sLog("Launching MonichEngine2D ver: %d.%d.%d. - %s.", revision::engine::GetVersionMajor(), revision::engine::GetVersionMinor(), revision::engine::GetVersionPatch(), revision::engine::GetVersionString());
    sLog("Used GIT Hash: %s.", revision::GetHash());
    START_LOAD("Loading Engine Data...\n");

    _SetActivated();

    if (MajorErrorEncountered())
    {
        //message wasnt printed - print it now
        _PrintErrorMessage();
        return false;
    }

    if (!IsChecked())
        ReportError(ME_WARNING_VERSION_NOT_CHECKED);

    if (!settings)
    {
        //settings not initialized, create new file
        settings = new Settings();
        ReportError(ME_WARNING_SETTINGS_UNINITIALIZED);
    }

    if (!settings->Load())
    {
        ReportError(ME_WARNING_NO_CONFIG_FILE);
        settings->GenerateDefault();

        if (!settings->Save())
            ReportError(ME_WARNING_CONFIG_FILE_NOT_SAVED);
    }

    success = _InitGLFW(windowName) && _InitGLEW() && _InitDevIL() && _InitShaders() && _InitFonts();

    if (success)
    {
        _SetInitialized();
		_SetPreparing(true);
        END_LOAD("Clint initialized successfully in %f ms.");
    }

    return success;
}

void MonichEngine::Terminate()
{
    _SetInitialized(false);

    if (errorType)
        sLog("WARNING! Engine was terminated with errors! Saved error: %d - %s.", errorType, GetErrorStringFor(errorType));
    else sLog("Engine terminating...");

    currentState = nullptr;
    nextState = nullptr;

    if (shaders)
        delete[] shaders;

    if (materials)
        delete[] materials;

    for (auto it = meshes.cbegin(); it != meshes.cend(); ++it)
        delete it->second;
    meshes.clear();

    if (settings)
    {
        settings->Save();
        delete settings;
    }

    if (IsActivated())
    {
        glDeleteVertexArrays(1, &vertexArrayObjectID);

        glfwTerminate();
    }

    sLog("Engine terminated succesfully. Thank you for playing! Please report any problems to me!");
    sLogInstance->terminate();
}

bool MonichEngine::SetNextState(State * nextState TODELETE)
{
    if (!nextState || (this->nextState && this->nextState.get()))
        return false;

    this->nextState = StatePtr(nextState);
    return true;
}

bool MonichEngine::HasStateFlag(MEstate flag) const
{
    return flag & stateFlag;
}

bool MonichEngine::IsInitialized() const
{
    return HasStateFlag(ENGINE_STATE_INITIALIZED);
}

bool MonichEngine::IsChecked() const
{
    return HasStateFlag(ENGINE_STATE_CHECKED);
}

bool MonichEngine::IsActivated() const
{
    return HasStateFlag(ENGINE_STATE_ACTIVATED);
}

bool MonichEngine::IsPreparing() const
{
    return HasStateFlag(ENGINE_STATE_PREPARING);
}

bool MonichEngine::IsForceQuiting() const
{
    return HasStateFlag(ENGINE_STATE_SHOULD_QUIT);
}

bool MonichEngine::IsUIRendering() const
{
    return HasStateFlag(ENGINE_STATE_UI_RENDERING);
}

bool MonichEngine::ErrorEncountered() const
{
    return errorType;
}

bool MonichEngine::MajorErrorEncountered() const
{
    return errorType > ME_ERROR_MAJOR_STAMP;
}

bool MonichEngine::MinorErrorEncountered() const
{
    return errorType && errorType < ME_ERROR_MAJOR_STAMP;
}

bool MonichEngine::IsReady() const
{
    return IsActivated() && !MajorErrorEncountered();
}

void MonichEngine::ReportError(MEerror errorType)
{
    if (errorType && !MajorErrorEncountered() && this->errorType < errorType)
    {
        this->errorType = errorType;
        _PrintErrorMessage();
    }
}

MEerror MonichEngine::GetError() const
{
    return errorType;
}

const char * MonichEngine::GetErrorString() const
{
    return GetErrorStringFor(GetError());
}

const char * MonichEngine::GetErrorStringFor(MEerror error)
{
    switch (error)
    {
    case ME_WARNING_CAMERA_UINITIALIZED:
        return "Camera wasn`t configured in the engine - coding error";
    case ME_WARNING_CONFIG_FILE_NOT_SAVED:
        return "Couldn`t save config.ini file";
    case ME_WARNING_LOG_INIT_FAILED:
        return "Log file couldn`t been loaded";
    case ME_WARNING_NO_CONFIG_FILE:
        return "Couldn`t find config.ini file - new context will be generated";
    case ME_WARNING_NOT_TERMINATED:
        return "Engine wasn`t terminated properly";
    case ME_WARNING_SETTINGS_UNINITIALIZED:
        return "Settings weren`t initialized in the engine - coding error";
    case ME_WARNING_VERSION_NOT_CHECKED:
        return "Version wasn`t checked - coding error";

    case ME_ERROR_DEVIL_INIT:
        return "DevIL failed to initialize - should be a reason to be happy irl, but not here";
    case ME_ERROR_GLEW_INIT:
        return "Failed to establish connection to your GPU. Check if your GPU works with OpenGL version 3.3";
    case ME_ERROR_GLFW_INIT:
        return "Graphic handler library failed to initialize - update your GPU driver";
    case ME_ERROR_GLFW_WINDOW_FAILED:
        return "Window couldn't be created - update your GPU driver";
    case ME_ERROR_MATERIALS_INIT_FAILED:
        return "Materials failed to init - coding error";
    case ME_ERROR_REQUESTED_MESH_NOT_IN_PREPARING_STATE:
        return "Game requested loading mesh in working state - it should do it in preparation phase - coding error.";
    case ME_ERROR_SHADER_FAILED_VARIABLE_LOAD:
        return "Shader failed to load variable addresses - coding error";
    case ME_ERROR_STATE_UNINITIALIZED:
        return "State instance wasn`t passed to the engine - coding error";
    case ME_ERROR_VERSION_INVALID:
        return "Version of the engine doesn`t match the version required by the game - download or compile another version of MonichEngine2D.dll";
        //SHADERS
    case ME_ERROR_SHADER_ZERO + SHADER_ERROR_NON_INITIALIZED:
        return "MSH instance failed to initialize buffer - coding error";
    case ME_ERROR_SHADER_ZERO + SHADER_ERROR_INVALID_ID:
        return "MSH couldn`t find shader code for compiling - coding error";
    case ME_ERROR_SHADER_ZERO + SHADER_ERROR_VERTEX_SHADER_COMPILATION_FAILED:
        return "MSH failed to compile vertex shader - coding error";
    case ME_ERROR_SHADER_ZERO + SHADER_ERROR_FRAGMENT_SHADER_COMPILATION_FAILED:
        return "MSH failed to compile fragment shader - coding error";
    case ME_ERROR_SHADER_ZERO + SHADER_ERROR_PROGRAM_LINK_FAILED:
        return "MSH linker failed - coding error";
        //END OF SHADERS
        //MESHES
    case ME_ERROR_MESH_ZERO + MESH_ERROR_FILE:
        return "Mesh file failed to open";
    case ME_ERROR_MESH_ZERO + MESH_ERROR_FACE_FORMAT:
	case ME_ERROR_MESH_ZERO + MESH_ERROR_LINE_FORMAT:
        return "Mesh file is malformed";
        //END OF MESHES
        //TEXTURES
    case ME_ERROR_TEXTURE_ZERO + TEXTURE_ERROR_OVERRIDE:
        return "Texture was about to be overridden - coding error";
    case ME_ERROR_TEXTURE_ZERO + TEXTURE_ERROR_FILE_NOT_FOUND:
        return "Texture file wasn`t found";
    case ME_ERROR_TEXTURE_ZERO + TEXTURE_ERROR_CONVERSION_FAILED:
        return "DevIL couldn`t convert texture";
        //END OF TEXTURES
        //FREETYPE
    case ME_ERROR_FREETYPE_ZERO + FREETYPE_ERROR_FONT_NOT_FOUND:
        return "Couldn`t find font file - check your config for address";
    case ME_ERROR_FREETYPE_ZERO + FREETYPE_ERROR_INIT_FAILED:
        return "Freetype couldn`t initialize fonts module - update your GPU driver";
    default:
        return "Error not in database";
    }
}

bool MonichEngine::HasNextMouseEvent()
{
	return mouseEvents.size();
}

MouseEvent * TODELETE MonichEngine::NextMouseEvent()
{
	auto res = mouseEvents.front();
	mouseEvents.pop_front();
	return res;
}

Settings * NULLABLE MonichEngine::ProvideSettings(Settings * settings NULLABLE TODELETE)
{
    Settings* old = this->settings;
    this->settings = settings;
    return old;
}

bool MonichEngine::CheckVersion(uint8 major, uint8 minor)
{
    _SetChecked(true);

    if (major != revision::engine::GetVersionMajor() || minor > revision::engine::GetVersionMinor())
    {
        ReportError(ME_ERROR_VERSION_INVALID);
        return false;
    }
    else return true;
}

void MonichEngine::SetCamera(Camera * camera NULLABLE TODELETE)
{
    if (this->camera)
        delete this->camera;
    this->camera = camera;
}

void MonichEngine::SetUICamera(Camera * camera NULLABLE TODELETE)
{
    if (UICamera)
        delete UICamera;
    UICamera = camera;
}

void MonichEngine::RequestPreparationModeOn()
{
	_SetPreparing();
}

void MonichEngine::RequestPreparationModeOff()
{
	_SetPreparing(false);
}

void MonichEngine::Launch()
{
    // FPS checking and diff
    uint64 lastFPSCheck;
    uint64 frameBeginTime;
    uint64 frameEndTime;
    uint32 framesLastSecond = 0;
    uint32 diff = 0;

    // We need all modules working by now
    if (!IsInitialized() || MajorErrorEncountered())
        return;

    if (!nextState.get())
    {
        ReportError(ME_ERROR_STATE_UNINITIALIZED);
        return;
    }

    // Change current state
    _SwitchStates();
	//_SetPreparing(false); - done by _SwitchStates()

    frameEndTime = lastFPSCheck = clock();

    // Let`s start ticking!
    while (!_ShouldClose())
    {
        frameBeginTime = frameEndTime;

        currentState->logic(diff);

        glClear(GL_COLOR_BUFFER_BIT);
        currentState->render(diff);

        _SetUIRendering(true);
        currentState->renderUI(diff);
        _SetUIRendering(false);

        glfwSwapBuffers(window);

		// Event preparation
		DELETEVECTOR(mouseEvents);
        glfwPollEvents();

        frameEndTime = clock();

        diff = static_cast<uint32>(frameEndTime - frameBeginTime);

        // FPS check
        framesLastSecond++;
        if (frameEndTime - lastFPSCheck >= 1000) { // If last prinf() was more than 1 sec ago
                                                   // printf and reset timer
            printf("%f ms/frame  \\ FPS:%d\n", 1000.0 / double(framesLastSecond), framesLastSecond);
            framesLastSecond = 0;
            lastFPSCheck = frameEndTime;
        }
    }
}

void MonichEngine::ForceStop()
{
    _SetShouldQuit();
}

Settings * NULLABLE MonichEngine::GetSettings()
{
    return settings;
}

Camera * NULLABLE MonichEngine::GetCamera()
{
    return camera;
}

Camera * NULLABLE MonichEngine::GetUICamera()
{
    return UICamera;
}

GLFWwindow * NULLABLE MonichEngine::GetWindow() const
{
    return window;
}

uint32 MonichEngine::LoadMesh(ccstring meshName)
{
	uint32 result;
	do {
		result = (uint32)rand();
	} while (meshes.find(result) != meshes.end());

	LoadMesh(result, meshName);
	return result;
}

Mesh * NULLABLE MonichEngine::LoadMesh(uint32 index, ccstring meshName)
{
    if (meshes[index])
        return nullptr;

    if (IsPreparing())
    {
        Mesh* m = new Mesh();
        if (MeshError err = m->Init(meshName))
        {
            ReportError(ME_ERROR_MESH_ZERO + err);
            sLog("MeshMgr: meshname: %s.", meshName);
            delete m;
        }
        else
        {
            // success
			meshes[index] = m;
            return m;
        }
    }
    else
    {
        ReportError(ME_ERROR_REQUESTED_MESH_NOT_IN_PREPARING_STATE);
        sLog("MeshMgr: meshName: %s.", meshName);
    }
    return nullptr;
}

Mesh * NULLABLE MonichEngine::LoadMesh(uint32 index, Mesh * newMesh NULLABLE TODELETE, bool deleteOld)
{
    auto finder = meshes.find(index);
    Mesh* oldMesh = finder != meshes.end() ? finder->second : NULL;

    meshes[index] = newMesh;

    if (oldMesh && deleteOld)
    {
        delete oldMesh;
        oldMesh = NULL;
    }

    return oldMesh;
}

Mesh * NULLABLE MonichEngine::GetMesh(uint32 index)
{
    auto finder = meshes.find(index);
    Mesh* res = finder != meshes.end() ? finder->second : NULL;
    return res;
}

void MonichEngine::Render(Object * o)
{
    _Render(o->GetMaterial(), o);
}

void MonichEngine::Render(Object ** o, uint32 objectCount)
{
	for (uint32 i = 0; i <= objectCount; i++)
		Render(o[i]);
}

void MonichEngine::RenderText(uint32 materialIndex)
{
    _Render(materialIndex, NULL);
}

void MonichEngine::StartMaterialsLoading(uint32 materialsCount)
{
    if (!materials && materialsCount)
    {
        currentMaterial = materialCount = materialsCount;

        materials = new Material*[materialsCount];

        for (uint32 i = 0; i < materialsCount; i++)
            materials[i] = NULL;
    }
}

void MonichEngine::LoadMaterial(Material * mat NULLABLE, uint32 index)
{
    materials[index] = mat;
}

bool MonichEngine::FinishMaterialsLoading()
{
    for (uint32 i = 0; i < materialCount; i++)
    {
        if (Material* matUsed = materials[i])
            if (!matUsed->Init(shaders[matUsed->shaderUsed]))
            {
                ReportError(ME_ERROR_MATERIALS_INIT_FAILED);
                sLog("MaterialsMgr: failed id: %d.", i);
                return false;
            }
    }

    return true;
}

void MonichEngine::PassMaterialData(uint32 materialIndex, uint32 dataIndex, uint64 data)
{
    _PassMaterialData(materialIndex, dataIndex, data);
}

void MonichEngine::PassMaterialData(uint32 materialIndex, uint32 dataIndex, float data)
{
    _PassMaterialData(materialIndex, dataIndex, data);
}

void MonichEngine::PassMaterialData(uint32 materialIndex, uint32 dataIndex, void* data)
{
    _PassMaterialData(materialIndex, dataIndex, data);
}

uint32 MonichEngine::LoadTexture(ccstring texName)
{
	uint32 result = textures.size();
	LoadTexture(result, texName);
	return result;
}

Texture *NULLABLE MonichEngine::LoadTexture(uint32 index, ccstring texName)
{
	std::string texStr(texName);

	auto res = textureMap.find(texStr);
	if (res != textureMap.end())
	{
		return textures[(*res).second];
	}
	else
	{
		if (IsPreparing())
		{
			Texture* t = new Texture();
			if (TextureError err = t->Init(texName))
			{
				ReportError(ME_ERROR_TEXTURE_ZERO + err);
				sLog("TexMgr: meshname: %s.", texName);
				delete t;
			}
			else
			{
				while (textures.size() <= index)
					textures.push_back(nullptr);
				// success
				textures[index] = t;
				textureMap.insert(std::make_pair(texStr, textures.size()));
				return t;
			}
		}
		else
		{
			ReportError(ME_ERROR_REQUESTED_MESH_NOT_IN_PREPARING_STATE);
			sLog("MeshMgr: meshName: %s.", texName);
		}
	}

	return nullptr;
}

Texture *NULLABLE MonichEngine::LoadTexture(uint32 index, Texture *tex NULLABLE TODELETE, bool deleteOld)
{
	Texture* old = NULL;

	if (textures.size() > index)
	{
		old = textures[index];
	}

	while (textures.size() <= index)
	{
		textures.push_back(NULL);
	}

	textures[index] = tex;

	if (old && deleteOld)
	{
		delete old;
		old = NULL;
	}

	return old;
}

Texture *NULLABLE MonichEngine::GetTexture(uint32 index)
{
	return textures[index];
}

Texture *NULLABLE MonichEngine::GetTexture(ccstring texNamed)
{
	auto finder = textureMap.find(texNamed);
	if (finder != textureMap.end())
		return GetTexture(finder->second);
	else return nullptr;
}

void MonichEngine::SetRenderTarget(RenderTarget * rTarget, bool clear)
{
	unsigned buffer = rTarget ? rTarget->GetBuffer() : 0;
	glBindFramebuffer(GL_FRAMEBUFFER, buffer);
	if(clear)
		glClear(GL_COLOR_BUFFER_BIT);
}

void MonichEngine::SetRenderViewPort(int32 x, int32 y, int32 w, int32 h)
{
	glViewport(x, y, w, h);
}

void MonichEngine::_SetFlag(MEstate flag, bool on)
{
    if (on)
        stateFlag |= flag;
    else stateFlag &= ~(flag);
}

void MonichEngine::_SetInitialized(bool on)
{
    _SetFlag(ENGINE_STATE_INITIALIZED, on);
}

void MonichEngine::_SetChecked(bool on)
{
    _SetFlag(ENGINE_STATE_CHECKED, on);
}

void MonichEngine::_SetActivated(bool on)
{
    _SetFlag(ENGINE_STATE_ACTIVATED, on);
}

void MonichEngine::_SetPreparing(bool on)
{
    _SetFlag(ENGINE_STATE_PREPARING, on);
}

void MonichEngine::_SetShouldQuit(bool on)
{
    _SetFlag(ENGINE_STATE_SHOULD_QUIT, on);
}

void MonichEngine::_SetUIRendering(bool on)
{
    _SetFlag(ENGINE_STATE_UI_RENDERING, on);
}

void MonichEngine::_PollMouseEvent(MouseEvent * TODELETE mEvent)
{
	mouseEvents.push_back(mEvent);
}

bool MonichEngine::_InitGLFW(ccstring windowName)
{
    START_LOAD("Initializing GLFW...");

    if (!glfwInit())
    {
        ReportError(ME_ERROR_GLFW_INIT);
        return false;
    }

    glfwWindowHint(GLFW_SAMPLES, 4); //multisampling x4
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //OpenGL version 3...
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3!
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL

                                                                   //setting the window
    window = glfwCreateWindow(static_cast<int>(settings->GetUnsignedData(SETTING_SCREEN_W)), static_cast<int>(settings->GetUnsignedData(SETTING_SCREEN_H)), windowName, NULL, NULL);
    if (window == NULL)
    {
        ReportError(ME_ERROR_GLFW_WINDOW_FAILED);
        return false;
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    //pass the window to GLEW
    glfwMakeContextCurrent(window);

	//make mouse listener
	glfwSetMouseButtonCallback(window, [](GLFWwindow* window, int button, int action, int mods) 
	{
		if (action == GLFW_PRESS)
		{
			float64 x, y;
			glfwGetCursorPos(window, &x, &y);
			MouseEvent* e = new MouseEvent(button, x, y);
			sEngine->_PollMouseEvent(e);
		}
	});

    END_LOAD("Initialized GLFW in %f ms.");

    return true;
}

bool MonichEngine::_InitGLEW()
{
    START_LOAD("Initializing GLEW...");

    glewExperimental = true;
    if (glewInit() != GLEW_OK) {
        ReportError(ME_ERROR_GLEW_INIT);
        return false;
    }

    // leaving those for 3D engine in the future
    //glEnable(GL_CULL_FACE);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glGenVertexArrays(1, &vertexArrayObjectID);
    glBindVertexArray(vertexArrayObjectID);

    //glEnable(GL_DEPTH_TEST);
    //glDepthFunc(GL_LESS);

    END_LOAD("Initialized GLFW in %f ms.");

    return true;
}

bool MonichEngine::_InitDevIL()
{
    START_LOAD("Initializing DevIL...");

    ilInit();
    ilClearColour(255, 255, 255, 000);

    ILenum ilError = ilGetError();
    if (ilError != IL_NO_ERROR)
    {
        ReportError(ME_ERROR_DEVIL_INIT);
        sLog("DevIL: %s.", iluErrorString(ilError));
        return false;
    }

    END_LOAD("Initialized DevIL in %f ms.");
    return true;
}

bool MonichEngine::_InitShaders()
{
    START_LOAD("Initializing shaders...");

    shaders = new Shader*[SHADER_TYPES_MAX];

    for (int i = 0; i < SHADER_TYPES_MAX; i++)
        shaders[i] = Shader::CreateNewInstance(i);

    cstring errorString = NULL;

    // It`s important - first create all shaders - then initialize them
    // It won`t have any problems with deleting that stuff
    for (int i = 0; i < SHADER_TYPES_MAX; i++)
    {
        if (Shader* shader = shaders[i])
        {
            if (ShaderError err = shader->InitShader(errorString))
            {
                ReportError(ME_ERROR_SHADER_ZERO + err);
                sLog("ShaderMgr - shader id = %d \t Error string was: %s.", i, errorString);
                return false;
            }
            if (int32 err = shader->AfterInit())
            {
                ReportError(ME_ERROR_SHADER_FAILED_VARIABLE_LOAD);
                sLog("ShaderMgr - shader id = %d \t Failed with code %d", i, err);
                return false;
            }
        }
        else sLog("ShaderMgr - WARNING - shader id = %d failed the creation!");
    }

    END_LOAD("Initialized shaders in %f ms.");
    return true;
}

bool MonichEngine::_InitFonts()
{
    if (int8 err = shaders[SHADER_TEXT]->ToTextMSH()->InitFreetype(settings))
    {
        ReportError(ME_ERROR_FREETYPE_ZERO + err);
        return false;
    }
    return true;
}

bool MonichEngine::_IsErrorMajor(MEerror errorType)
{
    return errorType > ME_ERROR_MAJOR_STAMP;
}

void MonichEngine::_PrintErrorMessage()
{
    const char* str = _IsErrorMajor(errorType) ? "ERROR: Graphic Engine detected major error: %d: %s" : "Minor error detected by GE: %d, %s";

    sLog(str, errorType, GetErrorStringFor(errorType));
}

void MonichEngine::_SwitchStates()
{
    if (nextState.get())
    {
        currentState = nextState;
        nextState = nullptr;

        _SetPreparing(true);
        currentState->prepare();
        _SetPreparing(false);
    }
}

bool MonichEngine::_ShouldClose()
{
    return IsForceQuiting() || glfwWindowShouldClose(window);
}

void MonichEngine::_Render(uint32 materialIndex, Object *o NULLABLE)
{
    Material* material = materials[materialIndex];

    if (materialIndex != currentMaterial)
    {
        currentMaterial = materialIndex;
        shaders[material->shaderUsed]->Use();
        material->Prepare();
    }

    glm::mat4* MVP = _GetMVP(o);

    material->Render(o, *MVP);

    delete MVP;
}

glm::mat4 * MonichEngine::_GetMVP(Object * o NULLABLE)
{
    using namespace glm;

    mat4* res = new mat4();
    Camera * cameraUsed = (HasStateFlag(ENGINE_STATE_UI_RENDERING) ? UICamera : camera);

    if (o)
    {
        mat4* modelMatrix = o->GetModelMatrix();
        (*res) = cameraUsed->GetCameraView() * (*modelMatrix);
        delete modelMatrix;
    }
    else (*res) = cameraUsed->GetCameraView();

    return res;
}
