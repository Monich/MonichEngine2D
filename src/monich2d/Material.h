#ifndef M2D_MATERIAL_H
#define M2D_MATERIAL_H

#include "Shader.h"
#include "Object.h"

//Materials are defined in GameData and are guarding rendering process
class Material
{
public:
    Material(unsigned shaderUsed) : shaderUsed(shaderUsed) {};
	virtual ~Material() {};

    //should load the entire Material, dont do it in constructor
    virtual bool Init(Shader* shaderUsed) = 0;
	virtual void Prepare() = 0;
	virtual void Render(Object* obj NULLABLE, glm::mat4& MVP) = 0;

    virtual void PassData(uint32 index, uint64 data) {};
    virtual void PassData(uint32 index, float data) {};
    virtual void PassData(uint32 index, void* data) {};

    const unsigned shaderUsed;
};

#endif
