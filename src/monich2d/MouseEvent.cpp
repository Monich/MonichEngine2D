#include "MouseEvent.h"

#include "Camera.h"
#include "MonichEngine.h"
#include "Settings.h"

#include <glfw\glfw3.h>

MouseEvent::MouseEvent(uint32 b, float64 x, float64 y) : x(x), y(y)
{
	switch (b)
	{
	case GLFW_MOUSE_BUTTON_LEFT:
		button = MOUSEBUTTON_LEFT;
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		button = MOUSEBUTTON_RIGHT;
		break;
	default:
		button = MOUSEBUTTON_ELSE;
	}
}
