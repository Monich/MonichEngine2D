#ifndef M2D_REV_H
#define M2D_REV_H

namespace revision
{
    namespace engine
    {
        int GetVersionMajor();
        int GetVersionMinor();
        int GetVersionPatch();
        const char * GetVersionString();
    }

    namespace project
    {
        int GetVersionMajor();
        int GetVersionMinor();
        int GetVersionPatch();
        const char * GetVersionString();
    }

    const char * GetHash();
}
#endif
