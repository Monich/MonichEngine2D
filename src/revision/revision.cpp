#include "revision.h"
#include "revisiondata.h"

#define MONICH_ENGINE_VERSION_MAJOR 1
#define MONICH_ENGINE_VERSION_MINOR 1
#define MONICH_ENGINE_VERSION_PATCH 2
#define MONICH_ENGINE_VERSION_STRING "RenderTarget update"

int revision::engine::GetVersionMajor()
{
    return MONICH_ENGINE_VERSION_MAJOR;
}

int revision::engine::GetVersionMinor()
{
    return MONICH_ENGINE_VERSION_MINOR;
}

int revision::engine::GetVersionPatch()
{
    return MONICH_ENGINE_VERSION_PATCH;
}

const char * revision::engine::GetVersionString()
{
    return MONICH_ENGINE_VERSION_STRING;
}

int revision::project::GetVersionMajor()
{
    return _PROJECT_VERSION_MAJOR;
}

int revision::project::GetVersionMinor()
{
    return _PROJECT_VERSION_MINOR;
}

int revision::project::GetVersionPatch()
{
    return _PROJECT_VERSION_PATCH;
}

const char * revision::project::GetVersionString()
{
    return _PROJECT_VERSION_STRING;
}

const char * revision::GetHash()
{
    return _HASH;
}
