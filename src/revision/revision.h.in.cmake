#ifndef M2D_REVISION_H
#define M2D_REVISION_H
 #define _REVISION                  "@rev_id_str@"
 #define _HASH                      "@rev_hash@"
 #define _DATE                      "@rev_date@"
 #define _BRANCH                    "@rev_branch@"
 
 #define _PROJECT_VERSION_MAJOR     @version_major@
 #define _PROJECT_VERSION_MINOR     @version_minor@
 #define _PROJECT_VERSION_PATCH     @version_patch@
 #define _PROJECT_VERSION_STRING    "@version_string@"
#endif // M2D_REVISION_H
