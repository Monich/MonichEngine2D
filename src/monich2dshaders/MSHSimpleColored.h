#ifndef MSH_SIMPLECOLORED_H
#define MSH_SIMPLECOLORED_H

#include <monich2d\Shader.h>

#include <glm\glm.hpp>

class Mesh;

//Simple shader that coloures the mesh in pure color
class MSHSimpleColored :
	public Shader
{
public:
	MSHSimpleColored();
    ~MSHSimpleColored() {};

	void SetColor(const glm::vec3& color);
    void Render(Mesh* mesh, glm::mat4 & MVPMatrix, int renderMode = 0x0004 /*GL_TRIANGLES*/);

protected:
    int32 AfterInit() override;
};

#endif