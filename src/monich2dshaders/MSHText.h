#ifndef MSH_TEXT_H
#define MSH_TEXT_H

#include <monich2d\Shader.h>

#include <glm\glm.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H

class Settings;

typedef uint8 FreeTypeError;
enum FreeTypeErrors
{
    FREETYPE_ERROR_NONE,
    FREETYPE_ERROR_INIT_FAILED,
    FREETYPE_ERROR_FONT_NOT_FOUND,
    FREETYPE_ERROR_MAX
};

class MSHText : public Shader
{
public:
    MSHText();
    ~MSHText();

    void Prepare();
    void SetSize(uint32 newSize);
    void Render(ccstring str, float x, float y, glm::mat4 & MVPMatrix);

    FreeTypeError InitFreetype(Settings* settings);

protected:
    int32 AfterInit() override;

private:
    FT_Library ft;
    FT_Face face;
    unsigned textureID;
    Settings* settings;
};

#endif
