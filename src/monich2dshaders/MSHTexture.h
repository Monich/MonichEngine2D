#ifndef MSH_TEXTURED_H
#define MSH_TEXTURED_H

#include <monich2d\Shader.h>

#include <glm\glm.hpp>

class Mesh;
class Texture;

//Shader that puts a single texutre on a mesh
class MSHTexture :
	public Shader
{
public:
	MSHTexture();
    ~MSHTexture() {};

	void SetTexture(Texture* tex);
    void Render(Mesh* mesh, glm::mat4 & MVPMatrix);

protected:
    int32 AfterInit() override;
};

#endif