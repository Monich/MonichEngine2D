#include "MSHSimpleColored.h"

#include <monich2d/Mesh.h>

#include <glew\glew.h>

enum ShaderFields
{
    FIELD_PROGRAM_ID,       // unsigned
    FIELD_VBO_HANDLE,       // attrib:vec2
    FIELD_MVP_MATRIX,       // uni:mat4
    FIELD_COLOR,            // uni:vec3
    FIELD_MAX
};

MSHSimpleColored::MSHSimpleColored() : Shader(SHADER_SIMPLE_COLOR)
{
    buffer = new int64[FIELD_MAX];
    for (uint8 i = 0; i < FIELD_MAX; i++)
        buffer[i] = 0;
}

int32 MSHSimpleColored::AfterInit()
{
    buffer[FIELD_VBO_HANDLE] = glGetAttribLocation(buffer[0], "aVBOHandle");
    buffer[FIELD_MVP_MATRIX] = glGetUniformLocation(buffer[0], "uMVPMatrix");
    buffer[FIELD_COLOR] = glGetUniformLocation(buffer[0], "uColor");

    for (int i = 1; i < FIELD_MAX; i++)
        if (buffer[i] == -1)
            return i;

    return 0;
}

void MSHSimpleColored::SetColor(const glm::vec3& color)
{
    glUniform3fv(buffer[3], 1, &color.x);
}

void MSHSimpleColored::Render(Mesh * mesh, glm::mat4 & MVPMatrix, int renderMode)
{
    glUniformMatrix4fv(GetBuffer(FIELD_MVP_MATRIX), 1, GL_FALSE, &MVPMatrix[0][0]);

    glEnableVertexAttribArray(buffer[FIELD_VBO_HANDLE]);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->GetBuffer(MESH_FIELD_VBO));

    glVertexAttribPointer(
        buffer[FIELD_VBO_HANDLE],               // address
        2,                                      // size
        GL_FLOAT,                               // type
        GL_FALSE,                               // normalized?
        0,                                      // stride
        (void*)0                                // array buffer offset
    );

    // indexing
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->GetBuffer(MESH_FIELD_IBO));
    glDrawElements(renderMode, mesh->GetBuffer(MESH_FIELD_INDICES), GL_UNSIGNED_INT, (void*)0);

    glDisableVertexAttribArray(buffer[FIELD_VBO_HANDLE]);
}
