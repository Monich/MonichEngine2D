#include "MSHTexture.h"

#include <monich2d\Mesh.h>
#include <monich2d\Texture.h>

#include <glew\glew.h>

enum ShaderFields
{
    FIELD_PROGRAM_ID,       // unsigned
    FIELD_VBO_HANDLE,       // attrib:vec2
    FIELD_MVP_MATRIX,       // uni:mat4
    FIELD_TEXTURE,          // uni:sample2D
    FIELD_UV_HANDLE,        // attrib:vec2
    FIELD_MAX
};

MSHTexture::MSHTexture() : Shader(SHADER_TEXTURE)
{
    buffer = new int64[FIELD_MAX];
    for (int8 i = 0; i < FIELD_MAX; i++)
        buffer[i] = 0;
}

void MSHTexture::SetTexture(Texture * tex)
{
    glBindTexture(GL_TEXTURE_2D, tex->GetTexture());
    glUniform1i(GetBuffer(FIELD_TEXTURE), 0);
}

void MSHTexture::Render(Mesh * mesh, glm::mat4 & MVPMatrix)
{
    glUniformMatrix4fv(GetBuffer(FIELD_MVP_MATRIX), 1, GL_FALSE, &MVPMatrix[0][0]);

    glEnableVertexAttribArray(GetBuffer(FIELD_VBO_HANDLE));
    glEnableVertexAttribArray(GetBuffer(FIELD_UV_HANDLE));

    //binding vbo
    glBindBuffer(GL_ARRAY_BUFFER, mesh->GetBuffer(MESH_FIELD_VBO));

    glVertexAttribPointer(
        GetBuffer(FIELD_VBO_HANDLE),   // address
        2,						// size
        GL_FLOAT,				// type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );

    //binding uvs
    glBindBuffer(GL_ARRAY_BUFFER, mesh->GetBuffer(MESH_FIELD_UVS));

    glVertexAttribPointer(
        GetBuffer(FIELD_UV_HANDLE),   // address
        2,						// size
        GL_FLOAT,				// type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );

    //indexing
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->GetBuffer(MESH_FIELD_IBO));
    glDrawElements(GL_TRIANGLES, mesh->GetBuffer(MESH_FIELD_INDICES), GL_UNSIGNED_INT, (void*)0);

    glDisableVertexAttribArray(GetBuffer(FIELD_VBO_HANDLE));
    glDisableVertexAttribArray(GetBuffer(FIELD_UV_HANDLE));
}

int32 MSHTexture::AfterInit()
{
	buffer[FIELD_VBO_HANDLE] = glGetAttribLocation(buffer[0], "aVBOHandle");
	buffer[FIELD_MVP_MATRIX] = glGetUniformLocation(buffer[0], "uMVPMatrix");
	buffer[FIELD_TEXTURE] = glGetUniformLocation(buffer[0], "uTextureUsed");
    buffer[FIELD_UV_HANDLE] = glGetAttribLocation(buffer[0], "aUV");

    for (int i = 1; i < FIELD_MAX; i++)
        if (buffer[i] == -1)
            return i;

    return 0;
}

