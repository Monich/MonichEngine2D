#include "MSHText.h"

#include <monich2d\Settings.h>

#include <glew\glew.h>

enum ShaderFields
{
    FIELD_PROGRAM_ID,       // unsigned,
    FIELD_PROJ_MATRIX,      // uni:mat4
    FIELD_MODEL_MATRIX,     // uni:mat4
    FIELD_VBO_HANDLE,       // attrib:vec2
    FIELD_UV_HANDLE,        // attrib:vec2
    FIELD_TEXTURE,          // uni:sample2D

    //not shader fields really 
    FIELD_INPUT_BOX,
    FIELD_INPUT_UV,

    FIELD_MAX
};

MSHText::MSHText() : Shader(SHADER_TEXT),
    ft(NULL), face(NULL), textureID(0), settings(NULL)
{
    buffer = new int64[FIELD_MAX];
    for (int i = 0; i < FIELD_MAX; i++)
        buffer[i] = 0;
}

MSHText::~MSHText()
{
    if(textureID)
        glDeleteTextures(1, &textureID);

    if (buffer)
        glDeleteBuffers(2, (GLuint*)(&buffer[FIELD_INPUT_BOX]));
}

void MSHText::Prepare()
{
    //glBindTexture(GL_TEXTURE_2D, textureID);
    //glUniform1i(buffer[FIELD_TEXTURE], 0);
}

void MSHText::SetSize(uint32 newSize)
{
    FT_Set_Pixel_Sizes(face, 0, newSize);
}

void MSHText::Render(ccstring str, float x, float y, glm::mat4 & MVPMatrix)
{
    glUniformMatrix4fv(GetBuffer(FIELD_PROJ_MATRIX), 1, GL_FALSE, &MVPMatrix[0][0]);

    //float screenW = settings->GetUnsignedData(SETTING_SCREEN_W);
    //float screenH = settings->GetUnsignedData(SETTING_SCREEN_H);

    //float sx = 2.0 / screenW;
    //float sy = 2.0 / screenH;
    //float nx = (2 * x / screenW) - 1;
    //float ny = (2 * y / screenH) - 1;

    FT_GlyphSlot g = face->glyph;

    ccstring p;

    glEnableVertexAttribArray(buffer[FIELD_VBO_HANDLE]);
    //glEnableVertexAttribArray(buffer[FIELD_UV]);

    //for (p = str; *p; p++)
    {
        //if (FT_Load_Char(face, *p, FT_LOAD_RENDER))
            //continue;

        /*glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            g->bitmap.width,
            g->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            g->bitmap.buffer
        );*/ 

        //float x2 = x + g->bitmap_left * sx;
        //float y2 = -y - g->bitmap_top * sy;
        //float w = g->bitmap.width * sx;
        //float h = g->bitmap.rows * sy;

        /*GLfloat box[4][2] = {
            { x,     -y - 1 },
            { x + 1, -y - 1 },
            { x,     -y },
            { x + 1, -y },
        };*/

        GLfloat box[4][2] = {
        { x - 50, y - 50 },
        { x + 50, y - 50 },
        { x - 50, y + 50 },
        { x + 50, y + 50 },
        };

        glBindBuffer(GL_ARRAY_BUFFER, buffer[FIELD_INPUT_BOX]);

        glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);

        glVertexAttribPointer(
            buffer[FIELD_VBO_HANDLE],
            2,						// size
            GL_FLOAT,				// type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
        );

        /*glBindBuffer(GL_ARRAY_BUFFER, buffer[FIELD_INPUT_UV]);

        glVertexAttribPointer(
            buffer[FIELD_UV],
            2,						// size
            GL_FLOAT,				// type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
        );*/

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        //nx += (g->advance.x / 64) * sx;
        //ny += (g->advance.y / 64) * sy;
    }

    glDisableVertexAttribArray(buffer[FIELD_VBO_HANDLE]);
    //glDisableVertexAttribArray(buffer[FIELD_UV]);
}

FreeTypeError MSHText::InitFreetype(Settings * settings)
{
    this->settings = settings;

    if (FT_Init_FreeType(&ft)) {
        return FREETYPE_ERROR_INIT_FAILED;
    }

     if (FT_New_Face(ft, settings->GetCStringData(SETTING_FONT_ADDRESS), 0, &face))
        return FREETYPE_ERROR_FONT_NOT_FOUND;
    
    // texture
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenBuffers(2, (GLuint*)(&buffer[FIELD_INPUT_BOX]));

    glBindBuffer(GL_ARRAY_BUFFER, buffer[FIELD_INPUT_UV]);
    GLfloat box[4][2] = {
        { 0, 1 },
        { 1, 1 },
        { 0, 0 },
        { 1, 0 },
    };
    glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_STATIC_DRAW);


    return FREETYPE_ERROR_NONE;
}

int32 MSHText::AfterInit()
{
    buffer[FIELD_PROJ_MATRIX] = glGetUniformLocation(buffer[0], "uProjMatrix");
    buffer[FIELD_MODEL_MATRIX] = 0;// glGetUniformLocation(buffer[0], "uModelMatrix");
    buffer[FIELD_VBO_HANDLE] = glGetAttribLocation(buffer[0], "aVBOHandle");
    buffer[FIELD_UV_HANDLE] = 0;// glGetAttribLocation(buffer[0], "aUV");
    buffer[FIELD_TEXTURE] = 0;// glGetUniformLocation(buffer[0], "uTextureUsed");

    for (int8 i = 0; i < FIELD_INPUT_BOX; i++)
        if (buffer[i] == -1)
            return i;

    return 0;
}
