#ifndef M2D_STATICCAMERA_H
#define M2D_STATICCAMERA_H

#include <monich2d\Camera.h>
#include <monich2d\defines.h>

class StaticCamera : public Camera
{
public:
    StaticCamera(uint32 w, uint32 h);
};

#endif