#include "StaticCamera.h"

#include <glm/gtc/matrix_transform.hpp>

StaticCamera::StaticCamera(uint32 w, uint32 h)
{
    cameraMatrix = glm::ortho(0.f, static_cast<float>(w), 0.f, static_cast<float>(h));
}
