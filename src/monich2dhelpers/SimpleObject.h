#ifndef M2D_SIMPLEOBJECT_H
#define M2D_SIMPLEOBJECT_H

#include <monich2d\Object.h>

class SimpleObject :
	public Object
{
protected:
	glm::vec2 position = glm::vec2(0);
	glm::vec2 rotation = glm::vec2(0);
	glm::vec2 scale = glm::vec2(1);

	Mesh* mesh;
	//material
	unsigned int mat;
public:
	SimpleObject();
	SimpleObject(Mesh * m, uint32 materialType);
	virtual ~SimpleObject();

	// Inherited via Object
	virtual Mesh * GetMesh() const override;
	virtual uint32 GetMaterial() override;
	virtual glm::mat4 * TODELETE GetModelMatrix() override;

	//setters
	void SetPosition(glm::vec2 newPos);
	void SetPosition(float x, float y);
	void SetRotation(glm::vec2 newRot);
	void SetRotation(float xRot, float yRot);
	void SetScale(glm::vec2 newScale);
	void SetScale(float newScale);
	void SetScale(float xScale, float yScale);
	void SetMesh(Mesh* newMesh);
	void SetMaterial(uint32 newMaterial);

	//getters
	glm::vec2 GetPosition() const;
	glm::vec2 GetRotation() const;
	glm::vec2 GetScale() const;
};

#endif