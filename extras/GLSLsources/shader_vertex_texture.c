#version 330 core
\n
in vec3 aVBOHandle;

uniform mat4 uMVPMatrix;

//TEXTURE COORDS

in vec2 aUV;
out vec2 iaUV;

void main()
{
    gl_Position = uMVPMatrix * vec4(aVBOHandle, 1);
	iaUV = aUV;
}