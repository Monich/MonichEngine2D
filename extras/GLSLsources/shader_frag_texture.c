#version 330 core

in vec2 iaUV;

uniform sampler2D uTextureUsed;

layout(location = 0) out vec3 color;

void main(){
    color = texture(uTextureUsed, iaUV).rgb;
}

