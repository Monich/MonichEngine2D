#version 330 core
\n
in vec2 aVBOHandle;

uniform mat4 uMVPMatrix;

void main()
{
    gl_Position = uMVPMatrix * vec4(aVBOHandle, 0, 1);
}