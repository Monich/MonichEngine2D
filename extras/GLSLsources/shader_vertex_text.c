#version 330 core
\n
uniform mat4 uProjectionMatrix;

in vec2 aVBOHandle;
//in vec2 aUV;

//out vec2 iaUV;

void main()
{
	gl_Position = uMVPMatrix * vec4(aVBOHandle, 0, 1);
	//iaUV = aUV;
}