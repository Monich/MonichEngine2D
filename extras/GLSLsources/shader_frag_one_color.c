#version 330 core
\n
uniform vec3 uColor;

layout(location = 0) out vec3 color;

void main()
{
    color = uColor;
}

