#version 330 core
\n
in vec3 meshHandle;
in vec3 centrePosition;
uniform mat4 uVPMatrix;
uniform vec3 uSize;

void main()
{
	vec3 position = meshHandle.xy + centrePosition*uSize;
	gl_Position = uVPMatrix * vec4(position, 1.0f);
}